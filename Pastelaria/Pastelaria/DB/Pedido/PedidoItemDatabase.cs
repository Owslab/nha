﻿using MySql.Data.MySqlClient;
using Pastelaria.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pastelaria.DB.Pedido
{
    class PedidoItemDatabase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_produto, id_pedido) VALUES (@id_produto, @id_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));
            parms.Add(new MySqlParameter("id_pedido", dto.IdPedido));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
