﻿using Pastelaria.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pastelaria
{
    public partial class Cadastro_de_produtos : Form
    {
        public Cadastro_de_produtos()
        {
            InitializeComponent();
        }

        private void Cadastro_de_produtos_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = new ProdutoDTO();
            dto.Nome = txtProduto.Text;
            dto.Preco = Convert.ToDecimal(txtPreco.Text);

            ProdutoBusiness business = new ProdutoBusiness();
            business.Salvar(dto);

            MessageBox.Show(" Produto salvo com sucesso.", "Pastelaria", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
